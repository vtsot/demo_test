<?php

namespace Acme\DemoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class TestController extends Controller
{
    
    /**
     * @Route("/", name="test")
     */
    public function indexAction(Request $request)
    {
        
        if ($request->query->get('v1') == 1) {
            
            $content = "covered v1 = 1";
            
        } elseif ($request->query->get('v1') == 2) {
            
            $content = "covered v1 = 2";
            
        } else {
            
            $content = "covered v1 not set";
            
        }
        
        return new Response($content);
    }
    
}
