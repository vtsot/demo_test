<?php

namespace Acme\DemoBundle\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CoverageTest extends WebTestCase 
{
    
    /**
     * получить массив проверяемых url
     * @return array(url => assertValue)
     */
    public function providerUrls()
    {
        return array(
            array('/test/?v1=1', 'v1 = 1'),
            array('/test/?v1=2', 'v1 = 2'),
        );
    }

    /** 
     * @dataProvider providerUrls 
     */
    public function testPageIsSuccessful($url, $assertValue)
    {
        $client = static::createClient();
        $client->request('GET', $url);
        $crawler = $client->getCrawler();
        $this->assertTrue($crawler->filter(':contains("'.$assertValue.'")')->count() > 0);
    }
        
}
