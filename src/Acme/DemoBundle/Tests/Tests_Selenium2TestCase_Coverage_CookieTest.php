<?php

namespace Acme\DemoBundle\Tests;

//define('PHPUNIT_TESTSUITE_EXTENSION_SELENIUM_HOST',     'locahost');
//define('PHPUNIT_TESTSUITE_EXTENSION_SELENIUM_PORT',     '4444');
//define('PHPUNIT_TESTSUITE_EXTENSION_SELENIUM_BROWSER',  '*firefox');
//define('PHPUNIT_TESTSUITE_EXTENSION_SELENIUM2_BROWSER', 'firefox');
//define('PHPUNIT_TESTSUITE_EXTENSION_SELENIUM_TESTS_URL', 'http://demo_test/app_dev.php');
//

//class Tests_Selenium2TestCase_Coverage_CookieTest extends \Tests_Selenium2TestCase_BaseTestCase
class Tests_Selenium2TestCase_Coverage_CookieTest extends \PHPUnit_Extensions_Selenium2TestCase

{

// this is a dummy URL (no server at that port), but Firefox still sets domain cookie, which is what's needed
    protected $coverageScriptUrl = 'http://127.0.0.1:8081/';

//    public static function setUpBeforeClass()
//    {
//        self::shareSession(true);
//    }

    public function setUp()
    {
        $this->setHost(PHPUNIT_TESTSUITE_EXTENSION_SELENIUM_HOST);
        $this->setPort((int)PHPUNIT_TESTSUITE_EXTENSION_SELENIUM_PORT);
        $this->setBrowser(PHPUNIT_TESTSUITE_EXTENSION_SELENIUM2_BROWSER);
        if (!defined('PHPUNIT_TESTSUITE_EXTENSION_SELENIUM_TESTS_URL')) {
            $this->markTestSkipped("You must serve the selenium-1-tests folder from an HTTP server and configure the PHPUNIT_TESTSUITE_EXTENSION_SELENIUM_TESTS_URL constant accordingly.");
        }
        $this->setBrowserUrl(PHPUNIT_TESTSUITE_EXTENSION_SELENIUM_TESTS_URL);
    }
    
//    
//    public function run(\PHPUnit_Framework_TestResult $result = NULL)
//    {
//// make sure code coverage collection is enabled
//        if ($result === NULL) {
//            $result = $this->createResult();
//        }
//        if (!$result->getCollectCodeCoverageInformation()) {
//            $result->setCodeCoverage(new PHP_CodeCoverage());
//        }
//        parent::run($result);
//        $result->getCodeCoverage()->clear();
//    }
//
//    protected function getTestIdCookie()
//    {
//        return $this->prepareSession()->cookie()->get('PHPUNIT_SELENIUM_TEST_ID');
//    }

    public function testTestIdCookieIsSet()
    {
        $this->url('/test/?v1=1');
//        return $this->getTestIdCookie();
    }

//    /**
//     * @depends testTestIdCookieIsSet
//     */
//    public function testTestsHaveUniqueTestIdCookies($previousTestIdCookie)
//    {
//        $this->url('/test/?v1=2');
////        $this->assertNotEquals($this->getTestIdCookie(), $previousTestIdCookie);
//    }

}
